// @flow
import React from 'react'
import PropTypes from 'prop-types'
import { square } from './square'

export const Simple = (props) => {

  const sqrResult = square(5)

  return (
    <main>
      <h1>Hello {props.title}</h1>
      <input type="text" value={sqrResult} readOnly/>
    </main>
  )
}

Simple.propTypes = {
  title: PropTypes.string
}
