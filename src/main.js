// @flow
import React from 'react'
import ReactDOM from 'react-dom'

import { Simple } from './simple.jsx'

ReactDOM.render(
  <Simple title="flow" />,
  document.getElementById('app')
)
